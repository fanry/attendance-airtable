FROM golang:1.17-alpine AS builder

WORKDIR /go/src/attendance

COPY backend-go/go.mod backend-go/go.sum ./
RUN go mod download

COPY backend-go .

RUN go build -o bin/ ./attendance

FROM alpine:latest AS app

ENV PORT=5000
EXPOSE 5000

COPY --from=builder /go/src/attendance/bin/attendance /app/
RUN adduser --uid 1000 --disabled-password --gecos '' --no-create-home goweb

WORKDIR /app
CMD /app/attendance

RUN chown goweb.goweb -R .
USER goweb
