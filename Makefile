
python-build-and-run: python-build-image python-run-container

python-build-image:
	podman build --file backend-python/Dockerfile -t attendance-python

python-run-container:
	podman run --env AIRTABLE_API_KEY=${AIRTABLE_API_KEY} --env AIRTABLE_BASE_ID=${AIRTABLE_BASE_ID} --env PORT=8000 -p 8000:8000 attendance-python

go-build-and-run: go-build-image go-run-container

go-build-image:
	podman build --file backend-go/Dockerfile -t attendance-go

go-run-container:
	podman run --env AIRTABLE_API_KEY=${AIRTABLE_API_KEY} --env AIRTABLE_BASE_ID=${AIRTABLE_BASE_ID} --env PORT=8000 -p 8000:8000 attendance-go
