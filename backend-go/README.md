It's not so straight forward with the pointers. Go automatically does some stuff, which makes it less obvious to remember
what is or not a pointer.

Missing telegram notifications


# Build

    go build -o bin/

# Testing

    go test

With coverage

    go test -cover

With report

    go test -coverprofile=coverage.out

With detailed report

    go test -coverprofile=coverage.out -covermode=atomic

Visualize report

    go tool cover -html=coverage.out

# Remarks
- mapstructure is to/from airtable
- json is to/from front-end

There are a couple of spots which doesn't look so nice because of different data expectations, but seems to work fine.

# Containers

Build

    podman build --file Dockerfile -t attendance-go

Run

    podman run --env AIRTABLE_API_KEY=key... --env AIRTABLE_BASE_ID=app... --env PORT=8000 -p 8000:8000 attendance-go

Stop

    podman stop <tab>

List images and containers: shows tags, and sizes

    podman images
    podman images --all
    podman image list
    podman container list --all

Delete

    podman rmi <tab>
    podman image rm <tab>
    podman image prune
    podman image prune --all
    podman container prune
