package api

import (
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"

	"attendance/attendance/repository"
)

type Route struct {
	URL     string
	Method  string
	Handler http.HandlerFunc
}

type Resource interface {
	GetRoutes() []Route
	GetProtectedRoutes() []Route
}

type APIServer struct {
	Repo            *repository.Repository
	Server          *http.Server
	router          *mux.Router
	protectedRouter *mux.Router
}

func NewAPI(repo *repository.Repository, addr string) *APIServer {
	router := mux.NewRouter()
	router.Use(CorsMiddleware)
	// router.Use(mux.CORSMethodMiddleware(router))
	auth := NewAuth(repo)
	protectedRouter := router.PathPrefix("/api").Subrouter()
	protectedRouter.Use(auth.Protect)
	protectedRouter.Use(CorsMiddleware)
	// protectedRouter.Use(mux.CORSMethodMiddleware(protectedRouter))

	apiServer := &APIServer{
		Repo: repo,
		Server: &http.Server{
			Addr:         addr,
			ReadTimeout:  5 * time.Second,
			WriteTimeout: 10 * time.Second,
			Handler:      router,
		},
		router:          router,
		protectedRouter: protectedRouter,
	}
	apiServer.Add(auth)
	apiServer.Add(NewOccurrences(repo))
	apiServer.Add(NewPasses(repo))
	apiServer.Add(NewAttendances(repo))

	return apiServer
}

func (a *APIServer) Add(resource Resource) {
	routes := resource.GetRoutes()

	for _, route := range routes {
		a.router.HandleFunc(route.URL, route.Handler).Methods(route.Method, http.MethodOptions)
	}

	protectedRoutes := resource.GetProtectedRoutes()
	for _, route := range protectedRoutes {
		a.protectedRouter.HandleFunc(route.URL, route.Handler).Methods(route.Method, http.MethodOptions)
	}
}

func CorsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		w.Header().Set("Access-Control-Allow-Methods", "*")
		if r.Method == http.MethodOptions {
			return
		}
		next.ServeHTTP(w, r)
	})
}

func (a *APIServer) Start() {
	log.Printf("APIServer.Start Starting the APIServer")
	err := a.Server.ListenAndServe()
	if err != nil {
		log.Printf("APIServer error: %s", err)
	}
}
