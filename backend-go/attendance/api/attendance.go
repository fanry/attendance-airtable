package api

import (
	"encoding/json"
	"log"
	"net/http"

	"attendance/attendance/models"
	"attendance/attendance/repository"

	"github.com/gorilla/mux"
)

type Attendances struct {
	repo *repository.Repository
}

func NewAttendances(repo *repository.Repository) *Attendances {
	Attendances := Attendances{
		repo: repo,
	}
	return &Attendances
}

func (a *Attendances) GetRoutes() []Route {
	return []Route{}
}

func (a *Attendances) GetProtectedRoutes() []Route {
	return []Route{
		{
			URL:     "/attendances",
			Method:  http.MethodGet,
			Handler: a.AttendancesHandler,
		},
		{
			URL:     "/attendance",
			Method:  http.MethodPost,
			Handler: a.CreateAttendancesHandler,
		},
		{
			URL:     "/attendance/{attendanceID}",
			Method:  http.MethodDelete,
			Handler: a.DeleteAttendancesHandler,
		},
	}
}

func (a *Attendances) AttendancesHandler(w http.ResponseWriter, _ *http.Request) {
	response, err := json.Marshal(&a.repo.Attendances)
	if err != nil {
		log.Printf("api.AttendancesHandler - Error: %s", err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	_, err = w.Write(response)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
	}
}

func (a *Attendances) CreateAttendancesHandler(w http.ResponseWriter, r *http.Request) {
	attendanceRequest := &models.Attendance{}
	if err := json.NewDecoder(r.Body).Decode(&attendanceRequest); err != nil {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}
	log.Printf("APIServer.CreateAttendancesHandler Got an attendance request for %+v", attendanceRequest)

	attendanceRequest.Occurrence = []string{attendanceRequest.OccurrenceID}
	attendanceRequest.Pass = []string{attendanceRequest.PassID}
	createdAttendance, err := a.repo.CreateAttendance(attendanceRequest)
	if err != nil {
		http.Error(w, "There was an error, try again.", http.StatusBadRequest)
		return
	}
	a.repo.Attendances = append(a.repo.Attendances, *createdAttendance)
	response, _ := json.Marshal(&createdAttendance)

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	_, err = w.Write(response)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
	}
}

func (a *Attendances) DeleteAttendancesHandler(w http.ResponseWriter, r *http.Request) {
	attendanceID := mux.Vars(r)["attendanceID"]
	attendance := a.repo.GetAttendanceByID(attendanceID)
	if attendance == nil {
		http.Error(w, "Attendance not found", http.StatusBadRequest)
		return
	}
	err := a.repo.DeleteAttendance(attendance)
	if err != nil {
		http.Error(w, "Could not remove attendance, try again", http.StatusBadRequest)
	}
	log.Printf("APIServer.DeleteAttendancesHandler Deleted attendance %+v", attendance)
}
