package api

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"attendance/attendance/models"
	"attendance/attendance/repository"

	"github.com/dchest/uniuri"
	"gopkg.in/validator.v2"
)

type Auth struct {
	repo *repository.Repository
}

type LoginRequest struct {
	Username string `json:"username" validate:"min=4,max=20,regexp=^[a-zA-Z0-9]*$"`
}

func NewAuth(repo *repository.Repository) *Auth {
	auth := Auth{
		repo: repo,
	}
	return &auth
}

func (a *Auth) GetRoutes() []Route {
	return []Route{
		{
			URL:     "/request",
			Method:  "POST",
			Handler: a.LoginHandler,
		},
	}
}

func (a *Auth) GetProtectedRoutes() []Route {
	return []Route{
		{
			URL:     "/check",
			Method:  "GET",
			Handler: a.CheckHandler,
		},
	}
}

func (a *Auth) LoginHandler(w http.ResponseWriter, r *http.Request) {
	loginRequest := LoginRequest{}
	if err := json.NewDecoder(r.Body).Decode(&loginRequest); err != nil {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}
	log.Printf("APIServer.LoginHandler Got a login request for %v", loginRequest.Username)
	if errs := validator.Validate(loginRequest); errs != nil {
		http.Error(w, fmt.Sprintf("Invalid Username '%v', try again", loginRequest.Username), http.StatusBadRequest)
		return
	}
	if user := a.repo.GetUserByUsername(loginRequest.Username); user != nil {
		http.Error(w, fmt.Sprintf("Invalid Username '%v', try again", loginRequest.Username), http.StatusBadRequest)
		return
	}
	token := uniuri.New()
	user := models.User{
		Username: loginRequest.Username,
		Token:    token,
		Approved: false,
	}
	createdUser, err := a.repo.CreateUser(&user)
	if err != nil {
		http.Error(w, "There was an error, try again.", http.StatusInternalServerError)
		return
	}
	a.repo.Users = append(a.repo.Users, *createdUser)
	response, _ := json.Marshal(&token)
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	_, err = w.Write(response)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
	}
}

func (a *Auth) CheckHandler(w http.ResponseWriter, _ *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	response, _ := json.Marshal(true)
	_, err := w.Write(response)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}
}

func (a *Auth) Protect(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		AuthHeader := r.Header.Get("Authorization")
		headerValue := strings.Split(AuthHeader, " ")
		if len(headerValue) != 2 {
			http.Error(w, "Not authorized", http.StatusUnauthorized)
			return
		}
		token := headerValue[1]
		if user := a.repo.GetUserByToken(token); user != nil {
			if user.Approved {
				log.Printf("Auth.Protect User %+v authenticated\n", user)
				next.ServeHTTP(w, r)
				return
			}
			// Try again in case cache is invalid
			a.repo.RefreshUsers()
			if user := a.repo.GetUserByToken(token); user != nil && user.Approved {
				log.Printf("Auth.Protect User %+v authenticated\n", user)
				next.ServeHTTP(w, r)
				return
			}
			log.Printf("Auth.Protect User %s not authorized\n", user.Username)
		}
		http.Error(w, "Not authorized", http.StatusUnauthorized)
	})
}

func (a *Auth) VerifyToken(token string) *models.User {
	if user := a.repo.GetUserByToken(token); user != nil && user.Approved {
		log.Printf("Auth.VerifyToken token is valid for user %+v\n", user)
		return user
	}
	return nil
}
