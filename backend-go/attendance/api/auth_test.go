package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"attendance/attendance/models"
	"attendance/attendance/repository"

	"github.com/gorilla/mux"
)

var repo *repository.Repository

func TestMain(m *testing.M) {
	repo = repository.NewRepository()
	code := m.Run()
	for _, user := range repo.Users {
		_, _ = repo.DB.Delete(&user)
	}
	os.Exit(code)
}

func TestLoginRequestHandler(t *testing.T) {
	auth := NewAuth(repo)
	handler := http.HandlerFunc(auth.LoginHandler)

	testUsernames := []struct {
		username   string
		shouldPass bool
	}{
		{"testUser", true},
		{"testUser34", true},
		{"testUser34", false},
		{"tes", false},
		{"testUsertestUsertestUser", false},
		{"tests user", false},
		{"tests-user", false},
	}

	for _, testItem := range testUsernames {
		requestBody, _ := json.Marshal(LoginRequest{Username: testItem.username})
		req, err := http.NewRequest("GET", "/request", bytes.NewBuffer(requestBody))
		if err != nil {
			t.Fatal(err)
		}
		recorder := httptest.NewRecorder()
		handler.ServeHTTP(recorder, req)

		var expectedStatus int
		switch testItem.shouldPass {
		case true:
			expectedStatus = http.StatusCreated
		case false:
			expectedStatus = http.StatusBadRequest
		}
		if status := recorder.Code; status != expectedStatus {
			t.Errorf("wrong status code: got %v want %v. %v",
				status, expectedStatus, recorder.Body.String())
		}

		expectedLen := 18
		if l := len(recorder.Body.String()); l != expectedLen && testItem.shouldPass {
			t.Errorf("unexpected token size: got %v (%v) want %v",
				len(recorder.Body.String()), recorder.Body.String(), expectedLen)
		}
	}
	//	Invalid json
	req, err := http.NewRequest("GET", "/request", bytes.NewBuffer([]byte("{'username':'myvalidusername'}")))
	if err != nil {
		t.Fatal(err)
	}
	recorder := httptest.NewRecorder()
	handler.ServeHTTP(recorder, req)
	if status := recorder.Code; status != http.StatusBadRequest {
		t.Errorf("wrong status code: got %v want %v. %v",
			status, http.StatusBadRequest, recorder.Body.String())
	}
}

func TestCheckHandler(t *testing.T) {
	auth := NewAuth(repo)
	repo.Users = append(repo.Users, []models.User{
		{
			Username: "testUser",
			Token:    "testToken",
			Approved: false,
		},
		{
			Username: "testUser",
			Token:    "testTokenApproved",
			Approved: true,
		},
	}...)
	router := mux.NewRouter()
	apiRouter := router.PathPrefix("/api").Subrouter()
	apiRouter.Use(auth.Protect)
	apiRouter.HandleFunc("/check", auth.CheckHandler)

	testTokens := []struct {
		token      string
		shouldPass bool
	}{
		{"testTokenApproved", true},
		{"testToken", false},
		{"doesntExist", false},
		{"", false},
	}

	for _, testItem := range testTokens {
		req, err := http.NewRequest("GET", "/api/check", nil)
		if err != nil {
			t.Fatal(err)
		}
		req.Header.Add("Authorization", fmt.Sprintf("Bearer %v", testItem.token))
		recorder := httptest.NewRecorder()
		apiRouter.ServeHTTP(recorder, req)

		var expectedStatus int
		switch testItem.shouldPass {
		case true:
			expectedStatus = http.StatusOK
		case false:
			expectedStatus = http.StatusUnauthorized
		}
		if status := recorder.Code; status != expectedStatus {
			t.Errorf("wrong status code: got %v want %v. %v",
				status, expectedStatus, recorder.Body.String())
		}
	}

	// No Authorization header
	req, err := http.NewRequest("GET", "/api/check", nil)
	if err != nil {
		t.Fatal(err)
	}
	recorder := httptest.NewRecorder()
	apiRouter.ServeHTTP(recorder, req)
	if status := recorder.Code; status != http.StatusUnauthorized {
		t.Errorf("wrong status code: got %v want %v. %v",
			status, http.StatusUnauthorized, recorder.Body.String())
	}
}
