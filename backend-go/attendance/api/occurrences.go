package api

import (
	"encoding/json"
	"log"
	"net/http"

	"attendance/attendance/repository"
)

type Occurrences struct {
	repo *repository.Repository
}

func NewOccurrences(repo *repository.Repository) *Occurrences {
	Occurrences := Occurrences{
		repo: repo,
	}
	return &Occurrences
}

func (o *Occurrences) GetRoutes() []Route {
	return []Route{}
}

func (o *Occurrences) GetProtectedRoutes() []Route {
	return []Route{
		{
			URL:     "/occurrences",
			Method:  "GET",
			Handler: o.OccurrencesHandler,
		},
	}
}

func (o *Occurrences) OccurrencesHandler(w http.ResponseWriter, _ *http.Request) {
	response, err := json.Marshal(o.repo.Occurrences)
	if err != nil {
		log.Printf("api.OccurrencesHandler - Error: %s", err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	_, err = w.Write(response)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
	}
}
