package api

import (
	"encoding/json"
	"log"
	"net/http"

	"attendance/attendance/repository"
)

type Passes struct {
	repo *repository.Repository
}

func NewPasses(repo *repository.Repository) *Passes {
	Passes := Passes{
		repo: repo,
	}
	return &Passes
}

func (p Passes) GetRoutes() []Route {
	return []Route{}
}

func (p Passes) GetProtectedRoutes() []Route {
	return []Route{
		{
			URL:     "/passes",
			Method:  "GET",
			Handler: p.PassesHandler,
		},
	}
}

func (p Passes) PassesHandler(w http.ResponseWriter, _ *http.Request) {
	response, err := json.Marshal(p.repo.Passes)
	if err != nil {
		log.Printf("api.PassesHandler - Error: %s", err)
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	_, err = w.Write(response)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
	}
}
