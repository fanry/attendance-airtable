package database

import (
	"log"
	"os"

	"github.com/mehanizm/airtable"
	"github.com/mitchellh/mapstructure"
)

type Database struct {
	client         *airtable.Client
	airtableAPIKey string
	airtableBaseID string
}

func NewDatabase() *Database {
	airtableAPIKey, apiKeyExists := os.LookupEnv("AIRTABLE_API_KEY")
	airtableBaseID, baseIDExists := os.LookupEnv("AIRTABLE_BASE_ID")
	if !apiKeyExists || len(airtableAPIKey) == 0 || !baseIDExists || len(airtableBaseID) == 0 {
		panic("Required env vars not set")
	}
	client := airtable.NewClient(airtableAPIKey)

	return &Database{
		client:         client,
		airtableAPIKey: airtableAPIKey,
		airtableBaseID: airtableBaseID,
	}
}

func (db *Database) getTable(name string) *airtable.Table {
	return db.client.GetTable(db.airtableBaseID, name)
}

func (db *Database) Create(entity Entity) (Entity, error) {
	tableName := entity.GetTableName()
	table := db.getTable(tableName)

	record := map[string]interface{}{}
	err := mapstructure.Decode(entity, &record)
	if err != nil {
		log.Printf("db.Create - Error decoding: %s", err)
		return nil, err
	}
	result, err := table.AddRecords(&airtable.Records{
		Records: []*airtable.Record{
			{
				Fields: record,
			},
		},
	})
	if err != nil || len(result.Records) == 0 {
		log.Printf("db.Create - Error: %s", err)
		return nil, err
	}
	createdEntity := entity.New()
	log.Printf("db.Create - result %+v", result.Records[0])
	err = mapstructure.Decode(result.Records[0].Fields, &createdEntity)
	if err != nil {
		log.Printf("db.Create - Error: %s", err)
		return nil, err
	}
	createdEntity.SetID(result.Records[0].ID)
	log.Printf("db.Create - createdEntity %+v", createdEntity)

	return createdEntity, err
}

func (db *Database) Update(entity Entity) (bool, error) {
	tableName := entity.GetTableName()
	table := db.getTable(tableName)

	record := map[string]interface{}{}
	err := mapstructure.Decode(entity, &record)
	if err != nil {
		log.Printf("db.Update - Error decoding: %s", err)
		return false, err
	}
	result, err := table.UpdateRecordsPartial(&airtable.Records{
		Records: []*airtable.Record{
			{
				ID:     entity.GetID(),
				Fields: record,
			},
		},
	})
	if err != nil || len(result.Records) == 0 {
		log.Printf("db.Update - Error: %s", err)
		return false, err
	}
	return true, err
}

func (db *Database) Delete(entity Entity) (bool, error) {
	tableName := entity.GetTableName()
	table := db.getTable(tableName)
	id := entity.GetID()
	if id == "" {
		log.Printf("db.Delete - Error: entity has no ID %v", entity)
		return false, nil
	}
	_, err := table.DeleteRecords([]string{entity.GetID()})
	if err != nil {
		log.Printf("db.Delete - Error: %s", err)
		return false, err
	}

	return true, nil
}

func (db *Database) GetAll(entity Entity) ([]Entity, error) {
	tableName := entity.GetTableName()
	tableView := entity.GetTableView()
	tableFields := entity.GetTableFields()
	table := db.getTable(tableName)

	var (
		entities   []Entity
		allRecords []*airtable.Record
		offset     string
	)

	for {
		records, err := table.GetRecords().
			FromView(tableView).
			ReturnFields(tableFields...).
			WithOffset(offset).
			Do()
		if err != nil {
			log.Printf("db.GetAll - Error: %s", err)
			return nil, err
		}
		offset = records.Offset
		allRecords = append(allRecords, records.Records...)
		if len(records.Offset) == 0 {
			break
		}
	}

	for _, record := range allRecords {
		newEntity := entity.New()
		err := mapstructure.Decode(record.Fields, &newEntity)
		if err != nil {
			log.Printf("db.GetAll - Error decoding: %s", err)
			continue
		}
		newEntity.SetID(record.ID)
		entities = append(entities, newEntity)
	}

	log.Printf("db.GetAll - Got %d %T", len(entities), entity)
	return entities, nil
}
