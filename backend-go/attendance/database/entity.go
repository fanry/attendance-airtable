package database

type Entity interface {
	New() Entity
	GetTableName() string
	GetTableView() string
	GetTableFields() []string
	SetID(id string)
	GetID() string
}
