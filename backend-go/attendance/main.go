package main

import (
	"fmt"
	"os"
	"time"

	"attendance/attendance/api"
	"attendance/attendance/repository"
)

func main() {
	repo := repository.NewRepository()
	go repo.RefreshIn(5 * time.Minute)
	repo.Refresh()

	port, ok := os.LookupEnv("PORT")
	if !ok {
		port = "8000"
	}

	server := api.NewAPI(repo, fmt.Sprintf(":%s", port))
	server.Start()
}
