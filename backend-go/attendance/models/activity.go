package models

import (
	"attendance/attendance/database"
)

type Activity struct {
	ID    string `mapstructure:"-"`
	Name  string `mapstructure:"Name"`
	Level string `mapstructure:"Level"`
}

func (u Activity) New() database.Entity {
	return &Activity{}
}

func (u Activity) GetTableName() string {
	return "Activities"
}

func (u Activity) GetTableView() string {
	return "Summary"
}

func (u Activity) GetTableFields() []string {
	return []string{"Name", "Level"}
}

func (u *Activity) SetID(id string) {
	u.ID = id
}

func (u *Activity) GetID() string {
	return u.ID
}
