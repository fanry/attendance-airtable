package models

import (
	"attendance/attendance/database"
)

type Attendance struct {
	ID           string   `json:"attendanceId" mapstructure:"-"`
	Name         string   `json:"name" mapstructure:"-"`
	Occurrence   []string `json:"-" mapstructure:"Occurrence"`
	OccurrenceID string   `json:"occurrenceId" mapstructure:"-"`
	Pass         []string `json:"-" mapstructure:"pass"`
	PassID       string   `json:"passId" mapstructure:"-"`
}

func (a Attendance) New() database.Entity {
	return &Attendance{}
}

func (a Attendance) GetTableName() string {
	return "AttenDance"
}

func (a Attendance) GetTableView() string {
	return "for attendance"
}

func (a Attendance) GetTableFields() []string {
	return []string{"Name", "Occurrence", "pass"}
}

func (a *Attendance) SetID(id string) {
	a.ID = id
}

func (a *Attendance) GetID() string {
	return a.ID
}
