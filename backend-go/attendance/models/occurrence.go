package models

import (
	"attendance/attendance/database"
)

type Occurrence struct {
	ID         string   `json:"occurrenceId" mapstructure:"-"`
	Name       string   `json:"name" mapstructure:"Name"`
	Activity   []string `json:"-" mapstructure:"Activity"`
	ActivityID string   `json:"activityId" mapstructure:"-"`
	Start      string   `json:"startDate" mapstructure:"Start"`
}

func (u Occurrence) New() database.Entity {
	return &Occurrence{}
}

func (u Occurrence) GetTableName() string {
	return "Occurrences"
}

func (u Occurrence) GetTableView() string {
	return "Today - for attendances"
}

func (u Occurrence) GetTableFields() []string {
	return []string{"Name", "Activity", "Start"}
}

func (u *Occurrence) SetID(id string) {
	u.ID = id
}

func (u *Occurrence) GetID() string {
	return u.ID
}
