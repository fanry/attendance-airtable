package models

import (
	"attendance/attendance/database"
)

type Pass struct {
	ID   string `json:"passId" mapstructure:"-"`
	Name string `json:"name" mapstructure:"Name"`

	Person     []string `json:"-" mapstructure:"Person"`
	PersonName string   `json:"personName" mapstructure:"-"`

	// Not used anymore in the frontend
	// ProductIds   []string `json:"-" mapstructure:"products"`
	// Products     string   `json:"products" mapstructure:"-"`

	Activity     []string `json:"-" mapstructure:"Activity"`
	ActivityID   string   `json:"activityId" mapstructure:"-"`
	ActivityName string   `json:"activityName" mapstructure:"-"`

	PassType   string   `json:"passType" mapstructure:"pass"`
	Activities []string `json:"activities" mapstructure:"activities"`
	Role       string   `json:"role" mapstructure:"role"`
	UsesLeft   string   `json:"usesLeft" mapstructure:"uses left"`

	OrderS      []string `json:"-" mapstructure:"order status"`
	OrderStatus string   `json:"orderStatus" mapstructure:"-"`
}

func (u Pass) New() database.Entity {
	return &Pass{}
}

func (u Pass) GetTableName() string {
	return "Passes"
}

func (u Pass) GetTableView() string {
	return "Passes for attendance"
}

func (u Pass) GetTableFields() []string {
	return []string{
		"Name",
		"Person",
		"Products",
		"Activity",
		"Pass",
		"Activities",
		"Role",
		"uses left",
		"order status",
	}
}

func (u *Pass) SetID(id string) {
	u.ID = id
}

func (u *Pass) GetID() string {
	return u.ID
}
