package models

import (
	"attendance/attendance/database"
)

type Person struct {
	ID       string `mapstructure:"-"`
	Name     string `mapstructure:"Name"`
	FullName string `mapstructure:"Full name"`
}

func (u Person) New() database.Entity {
	return &Person{}
}

func (u Person) GetTableName() string {
	return "People"
}

func (u Person) GetTableView() string {
	return ""
}

func (u Person) GetTableFields() []string {
	return []string{"Name", "Full name"}
}

func (u *Person) SetID(id string) {
	u.ID = id
}

func (u *Person) GetID() string {
	return u.ID
}
