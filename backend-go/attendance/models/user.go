package models

import (
	"attendance/attendance/database"
)

type User struct {
	ID       string `mapstructure:"-"`
	Username string `mapstructure:"username"`
	Token    string `mapstructure:"token"`
	Approved bool   `mapstructure:"approved"`
}

func (u User) New() database.Entity {
	return &User{}
}

func (u User) GetTableName() string {
	return "Login"
}

func (u User) GetTableView() string {
	return ""
}

func (u User) GetTableFields() []string {
	return []string{"username", "token", "approved"}
}

func (u *User) SetID(id string) {
	u.ID = id
}

func (u *User) GetID() string {
	return u.ID
}
