package repository

import (
	"log"
	"time"

	"attendance/attendance/database"
	"attendance/attendance/models"
)

type Repository struct {
	DB          *database.Database
	Users       []models.User
	Occurrences []models.Occurrence
	Passes      []models.Pass
	Persons     []models.Person
	Activities  []models.Activity
	Attendances []models.Attendance
	refreshedOn time.Time
}

func NewRepository() *Repository {
	db := database.NewDatabase()
	r := &Repository{
		DB: db,
	}
	return r
}

func (r *Repository) Refresh() {
	log.Println("Repository.Refresh Starting")
	start := time.Now()
	r.refreshedOn = start
	r.RefreshUsers()
	r.RefreshPersons()
	r.RefreshActivities()
	r.RefreshAttendances()
	r.RefreshPasses()
	r.RefreshOccurrences()
	duration := time.Since(start)
	log.Printf("Repository.Refresh Done in %0.2f seconds", duration.Seconds())
}

func (r *Repository) RefreshIn(duration time.Duration) {
	log.Printf("Repository.RefreshIn Scheduling refreshes every %0.2f seconds.", duration.Seconds())
	for {
		time.Sleep(duration / 10)
		refreshedLast := time.Since(r.refreshedOn)
		if refreshedLast < duration {
			log.Printf("Repository.RefreshIn Refreshed %0.2f seconds ago, sleeping for now...", refreshedLast.Seconds())
			continue
		}
		log.Printf("Repository.RefreshIn Last refresh was %0.2f seconds ago. Running scheduled refresh", refreshedLast.Seconds())
		r.Refresh()
	}
}
