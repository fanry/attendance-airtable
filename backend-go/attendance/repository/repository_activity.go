package repository

import (
	"log"

	"attendance/attendance/models"
)

func (r *Repository) RefreshActivities() bool {
	records, err := r.DB.GetAll(&models.Activity{})
	if err != nil {
		log.Println("Repository.RefreshActivities Couldn't refresh Activities")
		return false
	}
	var activities []models.Activity
	for _, record := range records {
		activity, ok := record.(*models.Activity)
		if !ok {
			log.Printf("Repository.RefreshActivities Invalid activity %v", activity)
			continue
		}
		activities = append(activities, *activity)
	}
	r.Activities = activities
	return true
}

func (r *Repository) GetActivityByID(id string) *models.Activity {
	for _, activity := range r.Activities {
		if activity.ID == id {
			return &activity
		}
	}
	return nil
}
