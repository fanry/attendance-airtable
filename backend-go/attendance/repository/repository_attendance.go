package repository

import (
	"log"

	"attendance/attendance/models"
)

func (r *Repository) RefreshAttendances() bool {
	records, err := r.DB.GetAll(&models.Attendance{})
	if err != nil {
		log.Println("Repository.RefreshAttendances Couldn't refresh Attendances")
		return false
	}
	var attendances []models.Attendance
	for _, record := range records {
		attendance, ok := record.(*models.Attendance)
		if !ok || len(attendance.Occurrence) == 0 || len(attendance.Pass) == 0 {
			log.Printf("Repository.RefreshAttendances Invalid attendance %v", attendance)
			continue
		}
		attendance.OccurrenceID = attendance.Occurrence[0]
		attendance.PassID = attendance.Pass[0]
		attendances = append(attendances, *attendance)
	}
	r.Attendances = attendances
	return true
}

func (r *Repository) GetAttendanceByID(id string) *models.Attendance {
	for _, attendance := range r.Attendances {
		if attendance.ID == id {
			return &attendance
		}
	}
	return nil
}

func (r *Repository) CreateAttendance(attendance *models.Attendance) (*models.Attendance, error) {
	created, err := r.DB.Create(attendance)
	createdAttendance, ok := created.(*models.Attendance)
	if err != nil || !ok {
		log.Printf("Repository.CreateAttendance - Could not create Attendance %+v", attendance)
		return nil, err
	}
	createdAttendance.OccurrenceID = attendance.OccurrenceID
	createdAttendance.PassID = attendance.PassID
	log.Printf("Repository.CreateAttendance - Created Attendance %+v", attendance)

	return created.(*models.Attendance), err
}

func (r *Repository) DeleteAttendance(attendance *models.Attendance) error {
	_, err := r.DB.Delete(attendance)
	if err != nil {
		log.Printf("Repository.DeleteAttendance - Could not delete Attendance %+v", attendance)
		return err
	}
	var newAttendances []models.Attendance

	for _, i := range r.Attendances {
		if i.ID != attendance.ID {
			newAttendances = append(newAttendances, i)
		}
	}
	r.Attendances = newAttendances
	return err
}
