package repository

import (
	"log"

	"attendance/attendance/models"
)

func (r *Repository) RefreshOccurrences() bool {
	records, err := r.DB.GetAll(&models.Occurrence{})
	if err != nil {
		log.Println("Repository.RefreshOccurrences Couldn't refresh Occurrences")
		return false
	}
	var occurrences []models.Occurrence
	for _, record := range records {
		occurrence, ok := record.(*models.Occurrence)
		if !ok {
			log.Printf("Repository.RefreshOccurrences Invalid occurrence %v", occurrence)
			continue
		}
		if aID := occurrence.Activity[0]; aID != "" {
			occurrence.ActivityID = aID
		}
		occurrences = append(occurrences, *occurrence)
	}
	r.Occurrences = occurrences
	return true
}
