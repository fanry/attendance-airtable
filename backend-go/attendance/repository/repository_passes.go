package repository

import (
	"log"

	"attendance/attendance/models"
)

func (r *Repository) RefreshPasses() bool {
	records, err := r.DB.GetAll(&models.Pass{})
	if err != nil {
		log.Println("Repository.RefreshPasses Couldn't refresh Passes")
		return false
	}
	var passes []models.Pass
	for _, record := range records {
		pass, ok := record.(*models.Pass)
		if !ok {
			log.Printf("Repository.RefreshPasses Invalid pass %v", pass)
			continue
		}
		if aID := pass.Activity[0]; aID != "" {
			pass.ActivityID = aID
			pass.ActivityName = r.GetActivityByID(aID).Name
		} else {
			log.Printf("Repository.RefreshPasses No ActivityID %v", pass)
		}
		if len(pass.OrderS) > 0 {
			if orderS := pass.OrderS[0]; orderS != "" {
				pass.OrderStatus = orderS
			} else {
				log.Printf("Repository.RefreshPasses No OrderStatus %v", pass)
			}
		}
		if len(pass.Person) > 0 {
			if personID := pass.Person[0]; personID != "" {
				pass.PersonName = r.GetPersonByID(personID).FullName
			} else {
				log.Printf("Repository.RefreshPasses No Person %v", pass)
			}
		}
		passes = append(passes, *pass)
	}
	r.Passes = passes
	return true
}
