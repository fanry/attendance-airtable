package repository

import (
	"log"

	"attendance/attendance/models"
)

func (r *Repository) RefreshPersons() bool {
	records, err := r.DB.GetAll(&models.Person{})
	if err != nil {
		log.Println("Repository.RefreshPersons Couldn't refresh Persons")
		return false
	}
	var persons []models.Person
	for _, record := range records {
		person, ok := record.(*models.Person)
		if !ok {
			log.Printf("Repository.RefreshPersons Invalid person %v", person)
			continue
		}
		persons = append(persons, *person)
	}
	r.Persons = persons
	return true
}

func (r *Repository) GetPersonByID(id string) *models.Person {
	for _, person := range r.Persons {
		if person.ID == id {
			return &person
		}
	}
	return nil
}
