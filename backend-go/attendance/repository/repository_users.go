package repository

import (
	"log"

	"attendance/attendance/models"
)

func (r Repository) GetUserByToken(token string) *models.User {
	for _, u := range r.Users {
		if u.Token == token {
			log.Printf("Repository.GetUserByToken Found user %s for token %s\n", u.Username, token)
			return &u
		}
	}
	log.Printf("Repository.GetUserByToken User not found for token %s\n", token)
	return nil
}

func (r Repository) GetUserByUsername(username string) *models.User {
	for _, u := range r.Users {
		if u.Username == username {
			log.Printf("Repository.GetUserByUsername Found user %s for username %s\n", u.Username, username)
			return &u
		}
	}
	log.Printf("Repository.GetUserByUsername User not found for username %s\n", username)
	return nil
}

func (r *Repository) RefreshUsers() bool {
	records, err := r.DB.GetAll(&models.User{})
	if err != nil {
		log.Println("Repository.RefreshUsers Couldn't refresh Users")
		return false
	}
	var users []models.User
	for _, record := range records {
		user, ok := record.(*models.User)
		if !ok {
			log.Printf("Repository.RefreshUsers Invalid user %v", user)
			continue
		}
		users = append(users, *user)
	}
	r.Users = users
	return true
}

func (r *Repository) CreateUser(user *models.User) (*models.User, error) {
	createdUser, err := r.DB.Create(user)
	if err != nil {
		log.Printf("repository.CreateUser - Could not create User %+v", user)
		return nil, err
	}

	return createdUser.(*models.User), err
}

func (r *Repository) ApproveUser(user *models.User) (bool, error) {
	user.Approved = true
	ok, err := r.DB.Update(user)
	if err != nil || !ok {
		log.Printf("repository.ApproveUser - Could not approve User %+v", user)
		return false, err
	}

	return true, err
}
