module attendance

go 1.16

require (
	github.com/dchest/uniuri v0.0.0-20200228104902-7aecb25e1fe5
	github.com/gorilla/mux v1.8.0
	github.com/mehanizm/airtable v0.2.5
	github.com/mitchellh/mapstructure v1.4.2
	github.com/stretchr/testify v1.7.0
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/validator.v2 v2.0.0-20210331031555-b37d688a7fb0
)
