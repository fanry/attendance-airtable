// +build integration

package attendance

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"testing"
	"time"
)

func TestAll(t *testing.T) {
	assrt := assert.New(t)
	assrt.Equal(0, len(s.Repo.Users))
	defer func() {
		for _, user := range s.Repo.Users {
			_, err := s.Repo.DB.Delete(&user)
			assrt.Nil(err)
		}
	}()

	testUsernames := []struct {
		username   string
		shouldPass bool
	}{
		{"testUser", true},
		{"testUser34", true},
		{"testUser34", false},
		{"tes", false},
		{"testUsertestUsertestUser", false},
		{"tests user", false},
		{"tests-user", false},
	}

	for i, testItem := range testUsernames {
		t.Run(fmt.Sprintf("TestUsername %d", i), func(t *testing.T) {
			requestBody, _ := json.Marshal(struct {
				Username string `json:"username"`
			}{
				Username: testItem.username,
			})
			response, err := http.Post(fmt.Sprintf("%s/request", serverAddress), "application/json", bytes.NewBuffer(requestBody))
			assrt.Nil(err)
			//defer response.Body.Close()

			data, err := ioutil.ReadAll(response.Body)
			assrt.Nil(err)

			var expectedStatus int
			expectedLen := 18
			switch testItem.shouldPass {
			case true:
				expectedStatus = http.StatusCreated
				assrt.Equal(expectedLen, len(data))
			case false:
				expectedStatus = http.StatusBadRequest
				assrt.NotEqual(expectedLen, len(data))
			}
			assrt.Equal(expectedStatus, response.StatusCode)
		})
	}
	// Invalid json
	response, err := http.Post(fmt.Sprintf("%s/request", serverAddress), "application/json", bytes.NewBuffer([]byte("{'username':'myvalidusername'}")))
	assrt.Nil(err)
	//defer response.Body.Close()
	assrt.Equal(http.StatusBadRequest, response.StatusCode)

	assrt.Equal(2, len(s.Repo.Users))
	approvedUser := &s.Repo.Users[0]
	ok, err := s.Repo.ApproveUser(approvedUser)
	assrt.Nil(err)
	assrt.Equal(true, ok)

	client := &http.Client{Timeout: time.Second * 1}

	for i, user := range s.Repo.Users {
		t.Run(fmt.Sprintf("TestLogin %d", i), func(t *testing.T) {
			request, err := http.NewRequest("GET", fmt.Sprintf("%s/api/check", serverAddress), nil)
			assrt.Nil(err)
			request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", user.Token))
			response, err := client.Do(request)
			assrt.Nil(err)
			//defer response.Body.Close()
			if user.Approved {
				assrt.Equal(http.StatusOK, response.StatusCode)
			} else {
				assrt.Equal(http.StatusUnauthorized, response.StatusCode)
			}
		})
	}

	for _, path := range []string{"/api/attendances", "/api/occurrences", "/api/passes"} {
		t.Run(fmt.Sprintf("Test %s", path), func(t *testing.T) {
			request, err := http.NewRequest("GET", fmt.Sprintf("%s%s", serverAddress, path), nil)
			assrt.Nil(err)
			request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", approvedUser.Token))
			//request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", s.Repo.Users[0].Token))
			response, err := client.Do(request)
			assrt.Nil(err)
			assrt.Equal(http.StatusOK, response.StatusCode)
			//defer response.Body.Close()
			body, err := ioutil.ReadAll(response.Body)
			assrt.Nil(err)
			var data []map[string]interface{}
			err = json.Unmarshal(body, &data)
			assrt.Nil(err)
			assrt.Greater(len(data), 0)
		})
	}
}
