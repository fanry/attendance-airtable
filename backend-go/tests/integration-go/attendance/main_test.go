// +build integration

package attendance

import (
	"fmt"
	"os"
	"testing"
	"time"

	"attendance/attendance/api"
	"attendance/attendance/models"
	"attendance/attendance/repository"
)

var s *api.APIServer

const serverAddress = "http://localhost:8000"

func NewTestServer() *api.APIServer {
	repo := repository.NewRepository()
	repo.Refresh()
	repo.Users = []models.User{}

	port, ok := os.LookupEnv("PORT")
	if !ok {
		port = "8000"
	}
	apiServer := api.NewAPI(repo, fmt.Sprintf(":%s", port))

	// wait for repo refresh with a timeout
	start := time.Now()
	for {
		if time.Since(start) > 10*time.Second {
			panic("Timeout for db refresh")
		}
		if len(apiServer.Repo.Occurrences) != 0 {
			break
		}
		time.Sleep(100 * time.Millisecond)
	}
	return apiServer
}

func TestMain(m *testing.M) {
	s = NewTestServer()
	go func() {
		err := s.Server.ListenAndServe()
		if err != nil {
			panic(err)
		}
		//defer s.Server.Close()
	}()
	os.Exit(m.Run())
}
