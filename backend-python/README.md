# AttendancePage backend
Airtable backend for the attendance app.

## Development

    poetry install

Then set all the env_vars and run the server:

    python bot.py

That already serves the frontend app at http://0.0.0.0:8000, but not with hot reload. The development version of the frontend can be started with:

    npm run start-ts
    npm run start
    npm run budo

`start-ts` converts the .ts files to .js in the build folder, while `start` bundles everything in one .js file. `budo` starts the dev server with hot-reload.

To use it in local dev environment, CORS need to be set-up on the server, and the requests have to be made to the same IP as the `budo` server uses. Set it as `backendUrl` in the `state.ts` file.

The production server uses statically build and committed files for the frontend, to run `npm run build` after setting `backendUrl` to the correct value and commit those files.


## Notifications
If set up, it can send telegram notifications using another app I created.
If you're also running that server, set up these vars to enable notifications:

- ACRA_TABLE_ID: get it from the URL when you open your ACRA table. It starts with "tbl"
- TELEGRAM_NOTIFICATION_URL: telegram-notifications server url
- CHAT_ID: telegram chat to send the message to

## Production
Pushing to Gitlab deploys to Netlify, and pushing to Dokku deploys the backend.

Set the env vars and run

    uvicorn backend:app --host=0.0.0.0 --port=8000 --log-level=info

## Dependencies
The main dependencies are:
- [pyAirtable: Airtable API for python](https://github.com/gtalarico/airtable-python-wrapper)
- [uvicorn: ASGI server](https://www.uvicorn.org/)
- [pydantic: data validation](https://pydantic-docs.helpmanual.io/)
- [ujson: parsing json message](https://github.com/ultrajson/ultrajson) (regular json package fails)
- [aiohttp: download images and attachments](https://docs.aiohttp.org/en/stable/)

Install with either

    pip install -Ur requirements.txt

or

    poetry install

# Container

    podman build --file Dockerfile -t attendance
    podmand run --env AIRTABLE_API_KEY=secret --env AIRTABLE_BASE_ID=secret --env PORT=8000 -p 8000:8000 attendance
    podman stop <tab>
