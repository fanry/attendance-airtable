# Copyright 2020 Felipe Morato me@fmorato.com
#
# This file is under the MIT License.
# See the file `LICENSE` for details.
import asyncio
import logging

from fastapi import Depends, FastAPI
from fastapi.middleware.cors import CORSMiddleware

from .airtable import create_login, refresh
from .api import api
from .auth import generate_token, validate_username, verify_credentials
from .config import ALLOWED_HOSTS, setup_logging
from .models import Login
from .tasks import refresh_in

setup_logging()
logger = logging.getLogger("attendance")
refresh_task: asyncio.Task

app = FastAPI(openapi_url=None, docs_url=None, redoc_url=None)
app.include_router(
    api,
    prefix="/api",
    dependencies=[Depends(verify_credentials)],
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=ALLOWED_HOSTS or ["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.post("/request")
async def request_login(login: Login = Depends(validate_username)):
    token = generate_token()
    create_login(username=login.username, token=token)
    return token


@app.on_event("startup")
async def startup():
    logger.info("Initializing")
    global refresh_task
    refresh()
    refresh_task = asyncio.create_task(refresh_in(5))
    logger.info("Initializing - done")


@app.on_event("shutdown")
async def shutdown():
    logger.info("Teardown")
    global refresh_task
    if refresh_task:
        refresh_task.cancel()
    logger.info("Teardown - Done")
