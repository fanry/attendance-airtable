import logging
import time
from secrets import compare_digest

from fastapi import HTTPException, status
from pyairtable import Table

from . import cache
from .config import AIRTABLE_API_KEY, AIRTABLE_BASE_ID
from .models import (
    Activity,
    Attendance,
    NewAttendance,
    Occurrence,
    Pass,
    Person,
    Product,
    Token,
)
from .utils import check_fields

logger = logging.getLogger("attendance.airtable")

people_table = Table(AIRTABLE_API_KEY, AIRTABLE_BASE_ID, "People")
passes_table = Table(AIRTABLE_API_KEY, AIRTABLE_BASE_ID, "Passes")
occurrences_table = Table(AIRTABLE_API_KEY, AIRTABLE_BASE_ID, "Occurrences")
products_table = Table(AIRTABLE_API_KEY, AIRTABLE_BASE_ID, "Products")
attendance_table = Table(AIRTABLE_API_KEY, AIRTABLE_BASE_ID, "AttenDance")
activities_table = Table(AIRTABLE_API_KEY, AIRTABLE_BASE_ID, "Activities")
login_table = Table(AIRTABLE_API_KEY, AIRTABLE_BASE_ID, "Login")


def refresh_passes():
    new_passes = {}
    data = passes_table.all(
        view="Passes for attendance",
        fields=[
            "Name",
            "Activity",
            "Products",
            "Pass",
            "Activities",
            "Role",
            "uses left",
            "Person",
            "order status",
        ],
    )
    for p in data:
        if not check_fields(
            ["Name", "Activity", "Products", "Pass", "Activities", "Role", "Person"], p
        ):
            continue
        if "uses left" not in p["fields"]:
            uses_left = 0
        elif p["fields"]["uses left"] == "∞":
            uses_left = -1
        else:
            uses_left = p["fields"]["uses left"]

        activity_id = ""
        if "Activity" in p["fields"]:
            activity_id = p["fields"]["Activity"][0]

        activity = cache.activities.get(activity_id, "")
        if activity_id and not activity:
            refresh_activities()
            activity = cache.activities.get(activity_id, "")
            if not activity:
                logger.error("Pass activity not found %s", activity_id)
                continue

        for rec in p["fields"]["Products"]:
            if rec not in cache.products.keys():
                refresh_products()
                break

        products = [
            cache.products[i].name.split(" - ")[0] for i in p["fields"]["Products"]
        ]
        person = cache.people.get(p["fields"]["Person"][0])
        new_passes[p["id"]] = Pass(
            passId=p["id"],
            name=p["fields"]["Name"],
            activityId=activity_id,
            activityName=activity.name,
            role=p["fields"]["Role"][0],
            passType=p["fields"]["Pass"],
            products=", ".join(products),
            personName=person.fullName,
            usesLeft=uses_left,
            activities=p["fields"]["Activities"],
            orderStatus=p["fields"]["order status"][0],
        )
    cache.passes.clear()
    cache.passes = new_passes

    logger.info("found %d passes", len(cache.passes.keys()))
    logger.debug("passes: %r", cache.passes)


def refresh_activities():
    new_activities = {}
    fields = ["Name", "Level"]
    data = activities_table.all(view="Summary", fields=fields)
    for a in data:
        if not check_fields(["Name"], a):
            continue
        new_activities[a["id"]] = Activity(
            activityId=a["id"],
            name=a["fields"]["Name"],
            level=a["fields"].get("Level", ""),
        )
    cache.activities.clear()
    cache.activities = new_activities

    logger.info("found %d activities", len(cache.activities.keys()))
    logger.debug("activities: %r", cache.activities)


def refresh_people():
    new_people = {}
    fields = ["Name", "Full name"]
    data = people_table.all(fields=fields)
    for p in data:
        if not check_fields(fields, p):
            continue
        new_people[p["id"]] = Person(
            personId=p["id"],
            name=p["fields"]["Name"],
            fullName=p["fields"]["Full name"],
        )
    cache.people.clear()
    cache.people = new_people
    logger.info(
        "found %d people",
        len(cache.people.keys()),
    )
    logger.debug("people: %r", cache.people)


def refresh_products():
    new_products = {}
    fields = ["Name", "Description"]
    data = products_table.all(fields=fields)
    for p in data:
        if not check_fields(["Name"], p):
            continue
        new_products[p["id"]] = Product(
            productId=p["id"],
            name=p["fields"]["Name"],
        )
    cache.products.clear()
    cache.products = new_products
    logger.info("found %d products", len(cache.products.keys()))
    logger.debug("products: %r", cache.products)


def refresh_occurrences():
    new_occurrences = {}
    fields = ["Name", "Activity", "Start"]
    data = occurrences_table.all(view="Today - for attendances", fields=fields)
    for o in data:
        if not check_fields(fields, o):
            continue

        name = o["fields"]["Name"]
        if isinstance(o["fields"]["Name"], list):
            name = ", ".join(o["fields"]["Name"])
        new_occurrences[o["id"]] = Occurrence(
            occurrenceId=o["id"],
            name=name,
            activityId=o["fields"]["Activity"][0],
            startDate=o["fields"]["Start"],
        )
    cache.occurrences.clear()
    cache.occurrences = new_occurrences
    logger.info("found %d occurrences", len(cache.occurrences.keys()))
    logger.debug("occurrences: %r", cache.occurrences)


def refresh_attendances():
    new_attendances = {}
    fields = ["Name", "Occurrence", "pass"]

    data = attendance_table.all(view="for attendance", fields=fields)
    for a in data:
        if not check_fields(fields, a):
            continue

        new_attendances[a["id"]] = Attendance(
            attendanceId=a["id"],
            name=a["fields"]["Name"],
            occurrenceId=a["fields"]["Occurrence"][0],
            passId=a["fields"]["pass"][0],
        )
    cache.attendances.clear()
    cache.attendances = new_attendances
    logger.info("found %d attendances", len(cache.attendances.keys()))
    logger.debug("attendances: %r", cache.attendances)


def refresh_tokens():
    new_tokens = {}
    fields = ["username", "token", "approved"]
    data = login_table.all(fields=fields)
    for t in data:
        if not check_fields(["username", "token"], t):
            continue

        token = t["fields"]["token"]
        new_tokens[token] = Token(
            tokenId=t["id"],
            username=t["fields"]["username"],
            token=token,
            approved=t["fields"].get("approved", False),
        )
    cache.tokens.clear()
    cache.tokens = new_tokens
    logger.info("found %d tokens", len(cache.tokens.keys()))
    logger.debug("tokens: %r", cache.tokens)


def refresh() -> None:
    logger.info("Refreshing")
    start = time.monotonic()
    refresh_tokens()
    refresh_people()
    refresh_products()
    refresh_occurrences()
    refresh_attendances()
    refresh_activities()
    refresh_passes()
    done = time.monotonic()
    cache.refreshed_on = done
    logger.info("Refreshing done in %f seconds", done - start)


def create_login(username: str, token: str) -> str:
    result = login_table.create({"username": username, "token": token})
    if not result:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Request failed, try again."
        )
    cache.tokens[token] = Token(
        tokenId=result["id"],
        username=username,
        token=token,
        approved=False,
    )
    return token


def remove_login(token: str) -> bool:
    token_id = [i for i, t in cache.tokens.items() if compare_digest(t.token, token)]
    if len(token_id) == 1:
        login_table.delete(token_id[0])
        return True
    logger.error("Did not find token_id for %s", token)
    return False


def create_attendance(new_attendance: NewAttendance):
    result = attendance_table.create(
        {
            "pass": [new_attendance.passId],
            "Occurrence": [new_attendance.occurrenceId],
        }
    )
    if not result:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Request failed, try again."
        )
    attendance = Attendance(
        attendanceId=result["id"],
        name=result["fields"]["Name"],
        passId=new_attendance.passId,
        occurrenceId=new_attendance.occurrenceId,
    )
    cache.attendances[attendance.attendanceId] = attendance
    attendance_pass = cache.passes[new_attendance.passId]
    if attendance_pass.usesLeft != -1:
        attendance_pass.usesLeft -= 1
    return attendance


def remove_attendance(attendance_id: str):
    attendance = cache.attendances.get(attendance_id, None)
    if not attendance:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Attendance not found."
        )
    if not attendance_table.delete(attendance_id):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Request failed, try again."
        )

    attendance_pass = cache.passes.get(attendance.passId)
    del cache.attendances[attendance_id]
    if attendance_pass and attendance_pass.usesLeft != -1:
        attendance_pass.usesLeft += 1
