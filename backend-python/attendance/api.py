import logging

from fastapi import APIRouter, BackgroundTasks, status

from . import cache
from .airtable import create_attendance, remove_attendance
from .models import NewAttendance
from .tasks import send_attendance_notification
from .utils import make_list

logger = logging.getLogger("attendance.api")
api = APIRouter()


@api.get("/check")
async def check_login():
    return True


@api.get("/passes")
async def get_passes():
    return make_list(cache.passes)


@api.get("/occurrences")
async def get_occurrences():
    return make_list(cache.occurrences)


@api.get("/attendances")
async def get_attendances():
    return make_list(cache.attendances)


@api.delete("/attendance/{attendance_id}", status_code=status.HTTP_204_NO_CONTENT)
async def delete_attendances(attendance_id: str, background_tasks: BackgroundTasks):
    attendance = cache.attendances.get(attendance_id)
    background_tasks.add_task(send_attendance_notification, attendance, deleted=True)
    remove_attendance(attendance_id)


@api.post("/attendance", status_code=status.HTTP_201_CREATED)
async def post_attendance(
    new_attendance: NewAttendance, background_tasks: BackgroundTasks
):
    attendance = create_attendance(new_attendance)
    background_tasks.add_task(send_attendance_notification, attendance)
    return attendance
