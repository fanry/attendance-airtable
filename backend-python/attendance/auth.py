import secrets
import string

from fastapi import Depends, HTTPException
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer

from . import cache
from .airtable import refresh_tokens
from .models import Login

ALPHABET = string.ascii_letters + string.digits
bearer = HTTPBearer()


def generate_token() -> str:
    return "".join(secrets.choice(ALPHABET) for _ in range(16))


def validate_username(login: Login) -> Login:
    for c in login.username:
        if c not in ALPHABET:
            raise HTTPException(status_code=402, detail="Invalid username, try again.")
    if login.username in {t.username for t in cache.tokens.values()}:
        raise HTTPException(status_code=402, detail="Invalid username, try again.")
    return login


def verify_token(token: str) -> bool:
    t = cache.tokens.get(token, None)
    if not t:
        return False
    if not t.approved:
        refresh_tokens()
    t = cache.tokens.get(token, None)
    if t and t.approved:
        return True
    return False


def verify_credentials(
    credentials: HTTPAuthorizationCredentials = Depends(bearer),
) -> bool:
    if not verify_token(credentials.credentials):
        raise HTTPException(status_code=401, detail="Invalid token, try again.")
    return True
