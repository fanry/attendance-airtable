from typing import Dict

from .models import Activity, Attendance, Occurrence, Pass, Person, Product, Token

passes: Dict[str, Pass] = {}
people: Dict[str, Person] = {}
activities: Dict[str, Activity] = {}
products: Dict[str, Product] = {}
attendances: Dict[str, Attendance] = {}
occurrences: Dict[str, Occurrence] = {}
tokens: Dict[str, Token] = {}
refreshed_on: float = 0
