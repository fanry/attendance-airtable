import logging
import logging.config
from typing import List

import yaml
from starlette.config import Config
from starlette.datastructures import CommaSeparatedStrings, Secret

logger = logging.getLogger("attendance.config")

config = Config()

# Required
AIRTABLE_API_KEY: str = config("AIRTABLE_API_KEY", cast=Secret)
AIRTABLE_BASE_ID: str = config("AIRTABLE_BASE_ID")

# Used for notification
ATTENDANCE_TABLE_ID: str = config("ATTENDANCE_TABLE_ID", default=None)
TELEGRAM_NOTIFICATION_URL: str = config("TELEGRAM_NOTIFICATION_URL", default=None)
CHAT_ID: int = config("CHAT_ID", cast=int, default=0)
ATTENDANCE_NOTIFICATION_TEMPLATE: str = (
    f"[{{event}}: {{name}}](https://airtable.com/{ATTENDANCE_TABLE_ID}/{{record_id}})"
)

# Other
ALLOWED_HOSTS: List[str] = config(
    "ALLOWED_HOSTS",
    cast=CommaSeparatedStrings,
    default="",
)


def setup_logging():
    with open("loggingConfig.yaml", "rt") as file:
        logging_config = yaml.safe_load(file.read())
        logging.config.dictConfig(logging_config)
