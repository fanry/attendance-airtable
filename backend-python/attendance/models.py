from typing import List, Optional

from pydantic import BaseModel


class Person(BaseModel):
    personId: str
    name: str
    fullName: str


class Pass(BaseModel):
    passId: str
    name: str
    personName: str
    activityId: Optional[str]
    activityName: Optional[str]
    role: Optional[str]
    passType: str
    usesLeft: int
    products: str
    activities: List[str]
    orderStatus: str


class Occurrence(BaseModel):
    occurrenceId: str
    name: str
    activityId: str
    startDate: str


class Product(BaseModel):
    productId: str
    name: str


class Activity(BaseModel):
    activityId: str
    name: str
    level: str


class Attendance(BaseModel):
    attendanceId: str
    name: str
    passId: str
    occurrenceId: str


class NewAttendance(BaseModel):
    passId: str
    occurrenceId: str


class Login(BaseModel):
    username: str


class Token(BaseModel):
    tokenId: str
    username: str
    token: str
    approved: bool
