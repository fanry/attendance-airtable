import asyncio
import logging
import time

from . import cache
from .airtable import refresh
from .config import ATTENDANCE_NOTIFICATION_TEMPLATE, CHAT_ID
from .models import Attendance
from .utils import send_notification

logger = logging.getLogger("attendance.tasks")


async def refresh_in(minutes: float):
    seconds = minutes * 60
    while True:
        try:
            await asyncio.sleep(60)
            refreshed_last = time.monotonic() - cache.refreshed_on
            if refreshed_last < seconds:
                logger.info(
                    "Refreshed %f seconds ago, sleeping for now...", refreshed_last
                )
                continue

            logger.info(
                "Last refresh was %f seconds ago. Running scheduled refresh",
                refreshed_last,
            )
            refresh()
        except asyncio.CancelledError:
            break


async def send_attendance_notification(attendance: Attendance, deleted=False):
    occurrence = cache.occurrences[attendance.occurrenceId]
    message = ATTENDANCE_NOTIFICATION_TEMPLATE.format(
        event=occurrence.name,
        name=attendance.name,
        record_id=attendance.attendanceId,
    )
    message = message if not deleted else f"**Deleted:** {message}"
    await send_notification(message, CHAT_ID)
