import logging
from typing import Dict

import ujson
from aiohttp import ClientConnectorError, ClientSession
from pydantic import BaseModel

from .config import CHAT_ID, TELEGRAM_NOTIFICATION_URL

logger = logging.getLogger("attendance.utils")


def check_fields(field_names, record):
    for field in field_names:
        if field not in record["fields"]:
            logger.debug("found record with missing field: %r", record)
            return False
    return True


def make_list(data: Dict[str, BaseModel]):
    return [i.dict() for i in data.values()]


async def send_notification(message: str, chat_id: int):
    if not (message or CHAT_ID or TELEGRAM_NOTIFICATION_URL):
        logger.info("Not sending notification. Is it configured?")
        return
    logger.info(
        "Sending notification through %r to %d", TELEGRAM_NOTIFICATION_URL, chat_id
    )
    session = ClientSession(
        headers={"User-Agent": "attendance/1.0"},
    )
    try:
        async with session.post(
            TELEGRAM_NOTIFICATION_URL,
            data=ujson.dumps(
                {"message": message, "chat_id": chat_id},
                ensure_ascii=False,
                escape_forward_slashes=False,
                indent=2,
            ),
        ) as r:
            if r.status == 204:
                logger.debug("message sent to %r: %r", chat_id, message)
                return True
            else:
                logger.debug("Failed to send message: %r", message)
                return False
    except ClientConnectorError as e:
        logger.error(e, exc_info=True)
        return None
    finally:
        await session.close()
