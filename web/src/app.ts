import m from "mithril";
import State from "./models/state";

import AttendancePage from "./components/AttendancePage";
import ActivitiesPage from "./components/ActivitiesPage";
import LoginPage from "./components/LoginPage";
import { Empty404 } from "./components/common";
import Auth from "./models/auth";
import "./app.css";

declare const ROUTE_PREFIX: string;

Auth.username = localStorage.getItem("username") || "";
Auth.token = localStorage.getItem("token") || "";
Auth.isLoggedIn = localStorage.getItem("isLoggedIn") === "true";

m.route.prefix = ROUTE_PREFIX;
m.route(document.body, "/", {
  "/": {
    onmatch: async () => {
      if (!Auth.isLoggedIn) return m.route.set("/login");
      await State.load();
    },
    render: () => m(ActivitiesPage),
  },
  "/attendance/:occurrenceId": {
    onmatch: async () => {
      if (!Auth.isLoggedIn) return m.route.set("/login");
      await State.load();
    },
    render: () => m(AttendancePage),
  },
  "/login": {
    onmatch: async () => {
      if (Auth.username && Auth.token && (await Auth.check()))
        return m.route.set("/");
    },
    render: () => m(LoginPage),
  },
  "/logout": {
    onmatch: () => {
      Auth.clear();
      return m.route.set("/login");
    },
  },
  "/:404...": {
    onmatch: () => (!Auth.isLoggedIn ? m.route.set("/login") : null),
    render: () => m(Empty404),
  },
});
