import m from "mithril";
import OccurrenceList, { Occurrence } from "../models/occurrence";

import { Empty, Footer, List, ListItem, TopBar } from "./common";
import State from "../models/state";

const todayIs = (date: string) => {
  const today = new Intl.DateTimeFormat("fi").format(new Date());
  return today === date;
};

interface OccurrenceListItemAttrs {
  title: string;
  occurrences: Occurrence[];
}

interface OccurrenceListAttrs {
  occurrencesByDate: Map<string, Occurrence[]>;
}

interface ActivityLinkAttrs {
  occurrence: Occurrence;
}

const ActivityLink: m.Component<ActivityLinkAttrs> = {
  view: ({ attrs: { occurrence } }) =>
    m(
      m.route.Link,
      { href: `/attendance/${occurrence.occurrenceId}` },
      occurrence.name
    ),
};

const OccurrenceListItem: m.Component<OccurrenceListItemAttrs> = {
  view: ({ attrs: { title, occurrences } }) => {
    return m(ListItem, {
      title: `${title}${todayIs(title) ? " - Today" : ""}`,
      detail: m(
        "ul.activities",
        occurrences.map((occurrence) =>
          m("li", m(ActivityLink, { occurrence }))
        )
      ),
    });
  },
};

const OccurrenceListView: m.Component<OccurrenceListAttrs> = {
  view: ({ attrs: { occurrencesByDate } }) =>
    m(
      List,
      Array.from(occurrencesByDate.keys()).map((title) => {
        const occurrence = occurrencesByDate.get(title);
        return occurrence
          ? m(OccurrenceListItem, {
              title,
              occurrences: occurrence,
            })
          : null;
      })
    ),
};

const ActivitiesPage: m.Component = {
  oninit: () => (document.title = "Activities"),
  view: () => {
    if (State.error) {
      return m(Empty, { content: "There was a network error." });
    }
    if (!OccurrenceList.all.length)
      return m(Empty, { content: "No activities" });

    return [
      m(TopBar, { title: "Activities" }),
      m(OccurrenceListView, {
        occurrencesByDate: OccurrenceList.byDate,
      }),
      m(Footer),
    ];
  },
};

export default ActivitiesPage;
