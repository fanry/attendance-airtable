import m, { Children } from "mithril";
import AttendanceList from "../models/attendance";
import OccurrenceList, { Occurrence } from "../models/occurrence";
import PassList, { Pass } from "../models/passes";
import State from "../models/state";

import { Empty, Footer, Icon, List, ListItem, TopBar } from "./common";

interface AttendanceItemAttrs {
  occurrenceId: string;
  pass: Pass;
}

interface AttendanceListAttrs {
  occurrenceId: string;
  passes: Pass[];
}

interface SearchBarAttrs {
  nPasses: number;
  nAttendees: number;
  nLeaders: number;
  nFollowers: number;
  nFollowerAttendees: number;
  nLeaderAttendees: number;
}

interface TabAttrs {
  occurrence: Occurrence;
  selectedOccurenceId: string;
}

interface TabsAttrs {
  occurrences: Occurrence[];
  selectedOccurenceId: string;
}

const AttendanceListItem: m.Component<AttendanceItemAttrs> = {
  view: ({ attrs: { occurrenceId, pass } }) => {
    const isPresent = AttendanceList.idFromPassIdAndOccurrenceId(
      pass.passId,
      occurrenceId
    );
    const isInfinite = pass.usesLeft == -1;
    const role =
      pass.role in ["F", "Follower"]
        ? "Follower"
        : pass.role in ["L", "Leader"]
        ? "Leader"
        : "";
    const klass =
      pass.role === "F"
        ? " text-primary"
        : pass.role === "L"
        ? "text-success"
        : "";
    let usesLeft = "pass";
    if (pass.usesLeft > 0) usesLeft = `| ${pass.usesLeft} left`;
    if (pass.usesLeft === 1 && pass.passType === "Once") usesLeft = "| unused";
    if (pass.usesLeft === 0) usesLeft = "| used";
    const orderStatus = pass.orderStatus === "🔴" ? "🔴" : "";

    return m(ListItem, {
      title: [
        m("strong", pass.personName),
        m(`small.${klass}`, ` ${orderStatus} ${role}`),
      ],
      detail: m("div", `${pass.passType} ${usesLeft} | ${pass.activityName}`),
      right: m(
        "label.form-checkbox",
        m("input[type=checkbox]", {
          disabled: pass.usesLeft === 0 && !isPresent,
          onclick: (e: Event) => {
            (<HTMLInputElement>e.target).checked
              ? AttendanceList.submitAttendance(pass.passId, occurrenceId).then(
                  () => !isInfinite && (pass.usesLeft -= 1)
                )
              : AttendanceList.removeAttendance(isPresent).then(
                  () => !isInfinite && (pass.usesLeft += 1)
                );
            PassList.all.filter(
              (passFromList) => pass.passId === passFromList.passId
            )[0].usesLeft = pass.usesLeft;
          },
          checked: isPresent && true,
        }),
        m("i.form-icon")
      ),
    });
  },
};

const AttendanceListView: m.Component<AttendanceListAttrs> = {
  view: ({ attrs: { occurrenceId, passes = [] } }) =>
    m(
      List,
      passes.map((pass) => m(AttendanceListItem, { occurrenceId, pass }))
    ),
};

const SearchBar: m.Component<SearchBarAttrs> = {
  view: ({
    attrs: {
      nPasses = 0,
      nAttendees = 0,
      nLeaders = 0,
      nFollowers = 0,
      nFollowerAttendees = 0,
      nLeaderAttendees = 0,
    },
  }) =>
    m(".search-bar has-icon-left bg-gray", [
      m(".search", [
        m(Icon, { icon: "search", form: true }),
        m(
          "input[type=search][name=search][autocomplete=off][placeholder=Search...].form-input.bg-gray",
          {
            oninput: (e: Event) =>
              AttendanceList.setSearch((<HTMLInputElement>e.target).value),
            value: AttendanceList.search,
            class: AttendanceList.search.length ? "active" : "",
          }
        ),
      ]),
      m(
        ".stats",
        `${nFollowers}F/${nLeaders}L (${nFollowerAttendees}F/${nLeaderAttendees}L)`
      ),
      m(
        ".stats",
        `${nAttendees}/${nPasses} (${Math.floor(
          (100 * nAttendees) / nPasses
        )}%)`
      ),
    ]),
};

const Tab: m.Component<TabAttrs> = {
  view: ({ attrs: { occurrence, selectedOccurenceId } }) => {
    const active =
      occurrence.occurrenceId === selectedOccurenceId ? "active" : "";
    return m(
      "li.tab-item",
      { class: active },
      m(
        "a",
        {
          class: active,
          onclick: () => m.route.set(`/attendance/${occurrence.occurrenceId}`),
        },
        occurrence.name
      )
    );
  },
};

const Tabs: m.Component<TabsAttrs> = {
  view: ({ attrs: { occurrences = [], selectedOccurenceId } }) =>
    m(
      "ul.tab tab-block",
      occurrences.map((occurrence) =>
        m(Tab, { occurrence, selectedOccurenceId })
      )
    ),
};

const AttendancePage = {
  view: (): Children => {
    if (State.error) m(Empty, { content: "There was a network error." });

    const occurrenceId: string = m.route.param("occurrenceId");
    const occurrence: Occurrence = OccurrenceList.byId.get(occurrenceId);

    if (!occurrence) {
      return m(Empty, {
        content: "Activity not found.",
        action: m(m.route.Link, { href: "/" }, "Go to Activities"),
      });
    }

    const occurrenceDate = new Intl.DateTimeFormat("fi").format(
      occurrence.datetime
    );
    const occurrencesOnDate = OccurrenceList.byDate.get(occurrenceDate);
    const title = `${occurrence.name} | ${occurrenceDate}`;
    document.title = title;
    const search = AttendanceList.search.toLowerCase();
    const occurrencePasses = PassList.all
      .filter((pass) => {
        if (occurrence.name.toLowerCase().includes("levels")) {
          return pass.activities.includes(occurrence.activityId);
        }
        return pass.activityId === occurrence.activityId;
      })
      .sort((a, b) => {
        const nameA = a.personName.toUpperCase();
        const nameB = b.personName.toUpperCase();
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }
        return 0;
      });
    const filteredPasses = occurrencePasses.filter((pass) =>
      pass.personName.toLowerCase().includes(search)
    );
    const occurrenceAttendees = AttendanceList.all.filter(
      (attendee) => attendee.occurrenceId === occurrenceId
    );
    const nFollowerAttendees = occurrenceAttendees.filter(
      (attendee) =>
        occurrencePasses.filter((pass) => pass.passId === attendee.passId)[0]
          .role == "F"
    ).length;
    const nLeaderAttendees = occurrenceAttendees.filter(
      (attendee) =>
        occurrencePasses.filter((pass) => pass.passId === attendee.passId)[0]
          .role == "L"
    ).length;

    return [
      m(TopBar, { title, backLink: "/" }),
      occurrencePasses.length
        ? [
            occurrencesOnDate.length > 1
              ? m(Tabs, {
                  occurrences: occurrencesOnDate,
                  selectedOccurenceId: occurrenceId,
                })
              : null,
            m(SearchBar, {
              nPasses: occurrencePasses.length,
              nAttendees: occurrenceAttendees.length,
              nFollowers: occurrencePasses.filter((pass) => pass.role === "F")
                .length,
              nLeaders: occurrencePasses.filter((pass) => pass.role === "L")
                .length,
              nFollowerAttendees: nFollowerAttendees,
              nLeaderAttendees: nLeaderAttendees,
            }),
            filteredPasses.length
              ? m(AttendanceListView, {
                  occurrenceId: occurrenceId,
                  passes: filteredPasses,
                })
              : m(Empty, { content: "No matches" }),
          ]
        : m(Empty, { content: "No participants" }),
      m(Footer),
    ];
  },
};

export default AttendancePage;
