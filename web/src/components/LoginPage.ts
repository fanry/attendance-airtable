import m, { Children, Vnode } from "mithril";
import State from "../models/state";
import Auth from "../models/auth";
import { Empty, EmptyAttrs, Icon } from "./common";

interface ButtonAttrs {
  click: (e: Event & { redraw: boolean }) => void;
  content: Children;
  buttonType?: "primary" | "link";
  buttonSize?: "sm" | "lg";
  buttonBlock?: boolean;
}

const Button: m.Component<ButtonAttrs> = {
  view: ({
    attrs: { click, content, buttonType, buttonSize, buttonBlock },
  }) => {
    const btnType = buttonType ? `.btn-${buttonType}` : "";
    const btnSize = buttonSize ? `.btn-${buttonSize}` : "";
    const btnBlock = buttonBlock ? ".btn-block" : "";
    return m(
      `button[type=button].btn${btnType}${btnSize}${btnBlock}`,
      { onclick: click },
      content
    );
  },
};

const LoginPage: m.Component = {
  oninit: Auth.check,
  view: (): Vnode<EmptyAttrs> | void => {
    if (State.error) {
      State.error = "";
      return m(Empty, {
        content: State.error.slice(),
        action: m(
          m.route.Link,
          { href: "/login" },
          m(Icon, { icon: "back" }),
          "  Return to login"
        ),
      });
    }
    if (Auth.username && Auth.token)
      return m(Empty, {
        content: m("", `Login requested for ${Auth.username}`),
        action: m("", [
          m(Button, {
            click: (e) => {
              e.redraw = false;
              Auth.check().then(() => m.route.set("/"));
            },
            content: "Validate",
            buttonType: "primary",
            buttonSize: "lg",
            buttonBlock: true,
          }),
          m(".my-2"),
          m(Button, { click: Auth.clear, content: "Reset", buttonSize: "sm" }),
        ]),
      });
    return m(Empty, {
      content: "Request a login",
      action: m(
        "form[action=#]",
        { onsubmit: Auth.login },
        m(".input-group", [
          m("input[type=text][required=required][autofocus=true].form-input", {
            oninput: (e: Event) =>
              Auth.setUsername((<HTMLInputElement>e.target).value),
            value: Auth.username,
          }),
          m(
            "input[type=submit][value=Request].btn.btn-primary.input-group-btn"
          ),
        ])
      ),
    });
  },
};

export default LoginPage;
