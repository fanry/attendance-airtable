import m, { Children } from "mithril";

export interface EmptyAttrs {
  content: Children;
  action?: Children;
}

const Empty: m.Component<EmptyAttrs> = {
  view: ({ attrs: { content, action } }) =>
    m(".empty", [
      m("p.empty-title h4", content),
      action ? m(".empty-action", action) : null,
    ]),
};

const Empty404: m.Component = {
  view: () =>
    m(Empty, {
      content: "Page not found",
      action: m(
        m.route.Link,
        { href: "/" },
        m(Icon, { icon: "back" }),
        " Return to app"
      ),
    }),
};

interface TopBarAttrs {
  title: Children;
  backLink?: string;
}

const TopBar: m.Component<TopBarAttrs> = {
  view: ({ attrs: { title, backLink } }) =>
    m(".topbar", [
      backLink
        ? m(
            m.route.Link,
            { href: backLink },
            m(Icon, {
              icon: "back",
              large: true,
            })
          )
        : m(
            m.route.Link,
            {
              href: "/logout",
              onclick: (e: KeyboardEvent) =>
                confirm("Logout?") ? true : e.preventDefault(),
            },
            m(Icon, {
              icon: "shutdown",
              large: true,
            })
          ),
      m(".title h3 text-center", title),
      m(".divider text-primary"),
    ]),
};

const Footer: m.Component = {
  view: () =>
    m(
      ".footer",
      m("p", m("a[href=https://forro.fi][target=_blank].text-dark", "forro.fi"))
    ),
};

interface IconAttrs {
  icon: // Navigation icons
  | "arrow-up"
    | "arrow-right"
    | "arrow-down"
    | "arrow-left"
    | "upward"
    | "forward"
    | "downward"
    | "back"
    | "caret"
    | "menu"
    | "apps"
    | "more-horiz"
    | "more-vert"
    // Action icons
    | "resize-horiz"
    | "resize-vert"
    | "plus"
    | "minus"
    | "cross"
    | "check"
    | "stop"
    | "shutdown"
    | "refresh"
    | "search"
    | "flag"
    | "bookmark"
    | "edit"
    | "delete"
    | "share"
    | "download"
    | "upload"
    | "copy"
    // Object icons
    | "mail"
    | "people"
    | "message"
    | "photo"
    | "time"
    | "location"
    | "link"
    | "emoji";
  form?: boolean;
  large?: boolean;
}

const Icon: m.Component<IconAttrs> = {
  view: ({ attrs: { icon, form, large } }) =>
    m(
      `i.${form ? "form-icon." : ""}icon${
        large ? " icon-2x " : " "
      }icon-${icon}`
    ),
};

interface ListItemAttrs {
  title: Children;
  detail: Children;
  right?: Children;
}

interface ListAttrs {
  children: Children;
}

const ListItem: m.Component<ListItemAttrs> = {
  view: ({ attrs: { title, detail, right } }) =>
    m(".list-item", [
      m(".item-content", [
        m(".item-title", title),
        m("small.item-detail", detail),
      ]),
      right,
    ]),
};

const List: m.Component<ListAttrs> = {
  view: ({ children }) => m(".list", children),
};

export { Empty, Empty404, Footer, List, ListItem, Icon, TopBar };
