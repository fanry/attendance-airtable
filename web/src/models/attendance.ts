import { apiRequest, processRequestError } from "../utils";

export interface Attendance {
  attendanceId: string;
  name: string;
  passId: string;
  occurrenceId: string;
}

interface AttendanceListType {
  search: string;
  all: Attendance[];
  setSearch: (value: string) => string;
  idFromPassIdAndOccurrenceId: (
    passId: string,
    occurrenceId: string
  ) => string | null;
  loadAttendances: () => Promise<void | Attendance[]>;
  submitAttendance: (
    passId: string,
    occurrenceId: string
  ) => Promise<void | Attendance>;
  removeAttendance: (attendanceId: string) => Promise<void | Attendance[]>;
  clear: () => void;
}

const AttendanceList: AttendanceListType = {
  search: "",
  all: [],
  setSearch: (value) => (AttendanceList.search = value),
  idFromPassIdAndOccurrenceId: (passId, occurrenceId) => {
    const attendance = AttendanceList.all.find(
      (a) => a.passId === passId && a.occurrenceId === occurrenceId
    );
    return attendance ? attendance.attendanceId : null;
  },
  loadAttendances: () =>
    apiRequest<Attendance[]>("GET", "/attendances")
      .then((result) => {
        AttendanceList.all = result ? result : [];
        return result;
      })
      .catch((er) => processRequestError(er)),
  submitAttendance: (passId, occurrenceId) =>
    apiRequest<Attendance>("POST", "/attendance", { passId, occurrenceId })
      .then((result) => {
        AttendanceList.all.push(result);
        return result;
      })
      .catch((er) => processRequestError(er)),
  removeAttendance: (attendanceId) =>
    apiRequest<Attendance[]>("DELETE", `/attendance/${attendanceId}`)
      .then(
        () =>
          (AttendanceList.all = AttendanceList.all.filter(
            (a) => a.attendanceId !== attendanceId
          ))
      )
      .catch((er) => processRequestError(er)),
  clear: () => (AttendanceList.all = []),
};
export default AttendanceList;
