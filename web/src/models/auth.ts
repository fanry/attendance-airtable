import m from "mithril";

import OccurrenceList from "./occurrence";
import AttendanceList from "./attendance";
import PassList from "./passes";
import State from "./state";
import { apiRequest, plainRequest } from "../utils";

interface AuthType {
  username: string;
  token: string;
  isLoggedIn: boolean;
  setUsername: (value: string) => string;
  login: (e: Event) => Promise<string | void>;
  check: () => Promise<boolean>;
  clear: () => void;
}

const Auth: AuthType = {
  username: "",
  token: "",
  isLoggedIn: false,
  setUsername: (value) => (Auth.username = value),
  login: (e) => {
    e.preventDefault();
    return plainRequest<string>("POST", "/request", { username: Auth.username })
      .then((token) => {
        Auth.token = token;
        localStorage.setItem("username", Auth.username);
        localStorage.setItem("token", token);
        m.route.set("/");
      })
      .catch((error) => {
        if (error.code === 402) State.error = error.response;
      });
  },
  check: () =>
    apiRequest<boolean>("GET", "/check")
      .then((result) => {
        Auth.isLoggedIn = result;
        localStorage.setItem("isLoggedIn", result.toString());
        return result;
      })
      .catch(() => {
        Auth.isLoggedIn = false;
        localStorage.removeItem("isLoggedIn");
        return false;
      }),
  clear: () => {
    localStorage.clear();
    Auth.username = "";
    Auth.token = "";
    Auth.isLoggedIn = false;
    AttendanceList.clear();
    OccurrenceList.clear();
    PassList.clear();
  },
};

export default Auth;
