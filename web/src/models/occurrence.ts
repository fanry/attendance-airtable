import { apiRequest, processRequestError } from "../utils";

export interface Occurrence {
  occurrenceId: string;
  name: string;
  activityId: string;
  startDate: string;
  datetime: Date;
}

interface OccurrenceListType {
  all: Occurrence[];
  byId: Map<string, Occurrence>;
  byDate: Map<string, Occurrence[]>;
  loadOccurrences: () => Promise<void | Occurrence[]>;
  clear: () => void;
}

const OccurrenceList: OccurrenceListType = {
  all: [],
  byId: new Map<string, Occurrence>(),
  byDate: new Map<string, Occurrence[]>(),
  loadOccurrences: () =>
    apiRequest<Occurrence[]>("GET", "/occurrences")
      .then((result) => {
        OccurrenceList.all = result ? result : [];
        OccurrenceList.byId.clear();
        OccurrenceList.byDate.clear();
        OccurrenceList.all.map((occurrence) => {
          OccurrenceList.byId.set(occurrence.occurrenceId, occurrence);
          const date = new Date(occurrence.startDate);
          const formattedDate = new Intl.DateTimeFormat("fi").format(date);
          if (OccurrenceList.byDate.has(formattedDate)) {
            OccurrenceList.byDate.set(formattedDate, [
              ...(OccurrenceList.byDate.get(formattedDate) || []),
              occurrence,
            ]);
          } else {
            OccurrenceList.byDate.set(formattedDate, [occurrence]);
          }
          return (occurrence.datetime = date);
        });
        return result;
      })
      .catch((er) => processRequestError(er)),
  clear: () => (OccurrenceList.all = []),
};

export default OccurrenceList;
