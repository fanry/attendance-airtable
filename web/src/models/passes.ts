import { apiRequest, processRequestError } from "../utils";

export interface Pass {
  passId: string;
  name: string;
  personName: string;
  products: string;
  activityId: string;
  activityName: string;
  passType: string;
  role: string;
  usesLeft: number;
  activities: string[];
  orderStatus: string;
}

interface PassListType {
  all: Pass[];
  loadPasses: () => Promise<void | Pass[]>;
  clear: () => void;
}

const PassList: PassListType = {
  all: [],
  loadPasses: () =>
    apiRequest<Pass[]>("GET", "/passes")
      .then((result) => {
        PassList.all = result ? result : [];
        return result;
      })
      .catch((er) => processRequestError(er)),
  clear: () => (PassList.all = []),
};

export default PassList;
