import OccurrenceList from "./occurrence";
import AttendanceList from "./attendance";
import PassList from "./passes";

interface StateType {
  loaded: boolean;
  error: string;
  load: () => Promise<boolean>;
}

const State: StateType = {
  loaded: false,
  error: "",
  load: () =>
    Promise.all([
      OccurrenceList.loadOccurrences(),
      AttendanceList.loadAttendances(),
      PassList.loadPasses(),
    ])
      .then(() => (State.loaded = true))
      .catch(() => {
        State.error = "There was a network error, try again.";
        return false;
      }),
};

export default State;
