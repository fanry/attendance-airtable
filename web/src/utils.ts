import m from "mithril";
import Auth from "./models/auth";

declare const BACKEND_URL: string;

interface RequestError extends Error {
  code: number;
  response: unknown;
}

const processRequestError = (error: RequestError): void => {
  if (error.code === 401) {
    if (Auth.isLoggedIn) Auth.clear();
    m.route.set("/login");
  }
};

const _request = <T>(
  method: string,
  path: string,
  body: any = null,
  headers: any = null
) =>
  m.request<T>({
    method: method,
    url: `${BACKEND_URL}${path}`,
    body: body,
    headers: headers,
  });

const plainRequest = <T>(
  method: string,
  path: string,
  body: any = null
): Promise<T> => _request<T>(method, path, body);

const apiRequest = <T>(
  method: string,
  path: string,
  body: any = null
): Promise<T> =>
  _request<T>(method, `/api${path}`, body, {
    Authorization: `Bearer ${Auth.token}`,
  });

export { plainRequest, apiRequest, processRequestError };
