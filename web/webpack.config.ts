import path = require("path");
import HtmlWebpackPlugin = require("html-webpack-plugin");
import MiniCssExtractPlugin = require("mini-css-extract-plugin");
import CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
import FaviconsWebpackPlugin = require("favicons-webpack-plugin");
import { DefinePlugin } from "webpack";

const isDevelopment = process.env.NODE_ENV === "development";

let extraConfig;
let envVars;
if (isDevelopment) {
  extraConfig = {
    devtool: "inline-source-map",
    devServer: {
      static: "./dist",
    },
  };
  envVars = {
    BACKEND_URL: JSON.stringify("http://localhost:8000"),
    ROUTE_PREFIX: JSON.stringify("#!"),
  };
} else {
  envVars = {
    BACKEND_URL: JSON.stringify("https://attendance.forro.fi"),
    ROUTE_PREFIX: JSON.stringify(""),
  };
}

export default {
  ...extraConfig,
  mode: process.env.NODE_ENV,
  entry: {
    app: "./src/app.ts",
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "Attendance",
      meta: {
        viewport:
          "user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no",
      },
    }),
    new FaviconsWebpackPlugin(
      path.resolve(__dirname, isDevelopment ? "icon-dev.svg" : "icon.svg")
    ),
    new DefinePlugin(envVars),
    new MiniCssExtractPlugin({
      filename: isDevelopment ? "[name].css" : "[name].[contenthash].css",
    }),
  ],
  output: {
    filename: "[name].[contenthash].js",
    path: path.resolve(__dirname, "dist"),
    clean: true,
    publicPath: "/",
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        include: path.resolve(__dirname, "src"),
      },
      {
        test: /\.css$/,
        use: [
          isDevelopment ? "style-loader" : MiniCssExtractPlugin.loader,
          "css-loader",
        ],
        include: path.resolve(__dirname, "src"),
      },
    ],
  },
  optimization: {
    minimizer: [new CssMinimizerPlugin(), "..."],
  },
};
